#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ethip_server = new ethernetip_server(this);
    tmr_updatescreen = new QTimer(this);
    tmr_updateplot = new QTimer(this);
    hora_atual = new QTime();
    data_atual = new QDate();
    ui->customPlot->setVisible(false);

    connect(ethip_server,SIGNAL(SendDatatoPlot(int,int,int,QString)),this,SLOT(CatchDatatoPlot(int,int,int,QString)));
    connect(tmr_updatescreen,SIGNAL(timeout()),this,SLOT(timer_updatescreen()));
    connect(tmr_updateplot,SIGNAL(timeout()),this,SLOT(timer_updateplot()));
    connect(ui->customPlot,SIGNAL(mouseRelease(QMouseEvent*)),this,SLOT(mouseReleased(QMouseEvent*)));

    ethip_server->StartServer();
    ui->temp1->setReadOnly(true);
    ui->press1->setReadOnly(true);
    ui->press2->setReadOnly(true);

    QFile checkusb("/udisk/log.csv");
    if(checkusb.exists())
    {
        ui->label_usb->setVisible(TRUE);
    }
    else
    {
        ui->label_usb->setVisible(FALSE);
    }

    tmr_updatescreen->start(1000);
    tmr_updateplot->start(60000);

    for (int i=0; i<60; ++i) // data for graph 0
    {
        temp[i] = 0;
        presi[i]= 0;
        prese[i]= 0;
    }
    PlotData(ui->customPlot);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::timer_updateplot()
{
    for(int x=0;x<59;x++)
    {
        temp[x] = temp[x+1];
        presi[x] = presi[x+1];
        prese[x] = prese[x+1];
    }
    temp[59] = tempe;
    presi[59] = pressi;
    prese[59] = presse;
    //if(ui->customPlot->isVisible())
    //{
        PlotData(ui->customPlot);
    //}
}

void MainWindow::mouseReleased(QMouseEvent *event)
{
    ui->customPlot->setVisible(false);
}

void MainWindow::PlotData(QCustomPlot *customPlot)
{
    customPlot->clearGraphs();
    customPlot->addGraph(customPlot->xAxis,customPlot->yAxis);
    customPlot->graph(0)->setPen(QPen(QColor(255, 102, 0)));
    customPlot->addGraph(customPlot->xAxis, customPlot->yAxis2);
    customPlot->graph(1)->setPen(QPen(QColor(170, 255, 0)));
    customPlot->addGraph(customPlot->xAxis, customPlot->yAxis2);
    customPlot->graph(2)->setPen(QPen(QColor(255, 255, 0)));
    QVector<double> x0(60), y0(60), y1(60), y2(60);
    for (int i=0; i<60; ++i)
    {
        x0[i] = i+1;
        y0[i] = temp[i];
        y1[i] = presi[i];
        y2[i] = prese[i];
    }
    customPlot->graph(0)->setData(x0, y0);
    customPlot->graph(1)->setData(x0, y1);
    customPlot->graph(2)->setData(x0, y2);
    customPlot->xAxis->setRange(1,60);
    customPlot->yAxis->setRange(0,130);
    customPlot->yAxis2->setRange(-2,4);
    customPlot->yAxis2->setVisible(true);
    customPlot->xAxis->setTickLength(0, 3);
    customPlot->xAxis->setSubTickLength(0, 1);
    customPlot->yAxis->setTickLength(0, 3);
    customPlot->yAxis->setSubTickLength(0, 1);
    customPlot->yAxis2->setTickLength(0, 3);
    customPlot->yAxis2->setSubTickLength(0, 1);
    customPlot->update();
    customPlot->replot();
}

void MainWindow::timer_updatescreen()
{
    QFile logfile("/udisk/log.csv");

    if(logfile.exists())
    {
        ui->label_usb->setVisible(TRUE);
    }
    else
    {
        ui->label_usb->setVisible(FALSE);
    }

    ui->label_hora->setText(hora_atual->currentTime().toString());   //.toString("HH:mm"));
}

void MainWindow::CatchDatatoPlot(int val1, int val2, int val3, QString lote)
{
    double v1,v2,v3;
    v1=(float)val1/10;
    v2=(float)val2/100;
    v3=(float)val3/100;
    ui->temp1->setText(QString::number(v1,'f',1));
    ui->press1->setText(QString::number(v2,'f',2));
    ui->press2->setText(QString::number(v3,'f',2));
    tempe = v1;
    pressi = v2;
    presse = v3;
    //qDebug() << "-------------\r\n" << tempe << "\r\n" << pressi << "\r\n" << presse << "\r\n";
}

void MainWindow::on_pushButton_showplot_clicked()
{
    ui->customPlot->setVisible(true);
}
