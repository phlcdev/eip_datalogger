#ifndef SERVER_THREAD_H
#define SERVER_THREAD_H

#include <QThread>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>

class server_thread : public QThread
{
    Q_OBJECT
public:
    explicit server_thread(QTcpSocket *sock, QObject *parent = 0);
    void run();

signals:
    void datasent(int val1, int val2, int val3, QString lote);
    void error(QTcpSocket::SocketError socketerror);

public slots:
    void readyRead();
    void disconnected();

private slots:
    int BytesToInt(QByteArray byteArr);

private:
    QString clp;
    QString lote;
    QTcpSocket *socket;
    int socketDescriptor;
};

#endif // SERVER_THREAD_H
