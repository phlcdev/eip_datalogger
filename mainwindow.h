#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QTime>
#include <QFile>
#include <QDebug>
#include <ethernetip_server.h>
#include "qcustomplot/qcustomplot.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public slots:
    void CatchDatatoPlot(int val1, int val2, int val3, QString lote);

private slots:
    void timer_updatescreen();
    void timer_updateplot();
    void PlotData(QCustomPlot *customPlot);
    void mouseReleased(QMouseEvent *event);
    void on_pushButton_showplot_clicked();

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    double temp[60],presi[60],prese[60];
    double tempe, pressi, presse;
    Ui::MainWindow *ui;
    ethernetip_server *ethip_server;
    QTimer *tmr_updatescreen;
    QTimer *tmr_updateplot;
    QTime *hora_atual;
    QDate *data_atual;
};

#endif // MAINWINDOW_H
