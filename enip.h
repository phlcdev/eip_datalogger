#ifndef ENIP_H
#define ENIP_H

#include <QThread>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>

class enip : public QThread
{
    Q_OBJECT
public:
    explicit enip(QObject *parent = 0);
    void run();

signals:
    void RetornaLote(QString slote);

public slots:
    void conecta();
    void disconnected();
    void connected();
    void ReadyRead();

private slots:


private:
    QString lote;
    QTcpSocket *socket;
    int socketDescriptor;
};

#endif // ENIP__H
