#include "ethernetip_server.h"

ethernetip_server::ethernetip_server(QObject *parent) :
    QTcpServer(parent)
{
    hora_atual = new QTime();
    data_atual = new QDate();
    tmr_logdata = new QTimer(this);
    // Conexao retorno do LOTE ------------
    thread_enip = new enip(this);
    //connect(thread_enip,SIGNAL(finished()),thread_enip,SLOT(deleteLater()));
    connect(thread_enip,SIGNAL(RetornaLote(QString)),this,SLOT(PegaLote(QString)));
    thread_enip->start();
    thread_enip->conecta();
    // ------------------------------------
    //qDebug() << data_atual->currentDate().toString("dd/MM/yyyy") << " " << hora_atual->currentTime().toString();
    valor1 = 0;
    valor2 = 0;
    valor3 = 0;
    ciclando=false;
    loteatual=false;
    connect(tmr_logdata,SIGNAL(timeout()),this,SLOT(timer_logdata()));

    tmr_logdata->start(60000);
}

void ethernetip_server::timer_logdata()
{
    QFile logfile("/udisk/log.csv");

    if(logfile.exists()&&ciclando==true)
    {
        // arquivo existe
        logfile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);
        QTextStream out(&logfile);
        out << data_atual->currentDate().toString("dd/MM/yyyy")
            << " " << hora_atual->currentTime().toString() << ";"
            << valor1 << ";"
            << valor2 << ";"
            << valor3 << ";"
            << lote << "\r\n";
        logfile.close();
    }
    if(ciclando==false) loteatual=false;
    qDebug() << "ciclando:" << ciclando;
    ciclando=false;
    //thread_enip->start();
    //thread_enip->conecta();
}

void ethernetip_server::StartServer()
{
    if(!this->listen(QHostAddress::Any,44818))
    {
        qDebug() << "Could not start the EtherNet-IP Server";
    } else
    {
        qDebug() << "EtherNet-IP Server listening";
    }
}

void ethernetip_server::PegaLote(QString slote)
{
    lote = slote;
    loteatual=true;
}

void ethernetip_server::incomingConnection(int socketDescriptor)
{
    //qDebug() << socketDescriptor << " Connecting...";
    QTcpSocket *temp_socket = new QTcpSocket();
    temp_socket->setSocketDescriptor(socketDescriptor);
    server_thread *thread = new server_thread(temp_socket, this);
    connect(thread,SIGNAL(datasent(int,int,int,QString)),this,SLOT(GetData(int,int,int,QString)));
    connect(thread,SIGNAL(finished()),thread,SLOT(deleteLater()));
    thread->start();
}

void ethernetip_server::GetData(int val1, int val2, int val3, QString slote)
{
    valor1 = (float)val1/10;
    valor2 = (float)val2/100;
    valor3 = (float)val3/100;
    if(loteatual==false) thread_enip->conecta();
    ciclando=true;
    //qDebug() << "Val1: " << val1 << " Val2: " << val2 << " Val3: " << val3  << " lote: " << slote;
    emit SendDatatoPlot(val1, val2, val3, slote);
}
