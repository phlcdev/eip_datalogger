#ifndef ETHERNETIP_SERVER_H
#define ETHERNETIP_SERVER_H

#include <QTcpServer>
#include <QDebug>
#include <QTimer>
#include <QTime>
#include <QFile>
#include <enip.h>
#include <server_thread.h>

class ethernetip_server : public QTcpServer
{
    Q_OBJECT
public:
    explicit ethernetip_server(QObject *parent = 0);
    void StartServer();

signals:
    void SendDatatoPlot(int val1, int val2, int val3, QString lote);

public slots:
    void GetData(int val1, int val2, int val3, QString lote);
    void PegaLote(QString slote);

private slots:
    void timer_logdata();

public:
    float valor1;
    float valor2;
    float valor3;
    QString lote;

private:
    bool ciclando;
    bool loteatual;
    enip *thread_enip;
    QTimer *tmr_logdata;
    QTime *hora_atual;
    QDate *data_atual;

protected:
    void incomingConnection(int socketDescriptor);

};

#endif // ETHERNETIP_SERVER_H
