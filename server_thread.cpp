#include "server_thread.h"

server_thread::server_thread(QTcpSocket *sock, QObject *parent) :
    QThread(parent)
{
    //this->socketDescriptor = ID;
    sock->moveToThread(this);
    socket = sock;
    //qDebug() << socket->peerAddress().toString();
    clp = "192.168.1.253";
}

void server_thread::run()
{
    //qDebug() << socketDescriptor << " Thread started";
    //socket = new QTcpSocket();
    /*if(!socket->setSocketDescriptor(this->socketDescriptor))
    {
        emit error(socket->error());
    }
    */
    connect(socket,SIGNAL(readyRead()),this,SLOT(readyRead()),Qt::DirectConnection);
    connect(socket,SIGNAL(disconnected()),this,SLOT(disconnected()),Qt::DirectConnection);

    //qDebug() << socketDescriptor << " Client connected";
    exec();
}

int server_thread::BytesToInt(QByteArray byteArr)
{
    QDataStream ds(&byteArr,QIODevice::ReadOnly);
    ds.setByteOrder(QDataStream::LittleEndian);

    int ret;
    qint16 tmp;
    ds >> tmp;
    ret = tmp;

    return ret;
}

void server_thread::readyRead()
{
    QByteArray datain;
    QByteArray dataout;
    QByteArray intconv;

    socket->setReadBufferSize(100);
    //socket->waitForReadyRead();
    datain = socket->readLine();
    //qDebug() << "[IN]" << datain.size() << " | " << datain.toHex();
    if(datain.at(0)=='\x65')
    {
        dataout = datain.mid(0,4);
        dataout.append(0x01);
        dataout.append(datain.mid(5,23));
        //qDebug() << "[OU]" << dataout.size() << " | " << dataout.toHex();
        socket->write(dataout);
        socket->flush();
        socket->waitForBytesWritten(1000);
        socket->waitForReadyRead(3000);
        datain.clear();
        datain = socket->readLine();
        socket->waitForReadyRead(3000);
        datain.append(socket->readLine());
        //qDebug() << "[IN]" << datain.size() << " | " << datain.toHex();
    }
    if(datain.at(0)=='\x6F')
    {
        dataout.clear();
        dataout.append('\x6F');
        dataout.append('\x00');
        dataout.append('\x2E');
        dataout.append(datain.mid(3,35));
        dataout.append('\x1E');
        dataout.append('\x00');
        dataout.append('\xD4');
        dataout.append('\x00');
        dataout.append('\x00');
        dataout.append('\x00');
        dataout.append('\x01');
        dataout.append('\x00');
        dataout.append('\x00');
        dataout.append('\x80');
        dataout.append(datain.mid(52,12)).append(datain.mid(68,4));
        dataout.append(datain.mid(74,3));
        dataout.append('\x00');
        dataout.append('\x00');
        dataout.append('\x00');
        //qDebug() << "[OU]" << dataout.size() << " | " << dataout.toHex();
        socket->write(dataout);
        socket->flush();
        socket->waitForReadyRead(5000);
        datain.clear();
        datain = socket->readAll();
        //qDebug() << "[IN]" << datain.size() << " | " << datain.toHex();
    }
    if(datain.at(0)=='\x70' && datain.size()==60)
    {
        intconv = datain.right(6);
        //qDebug() << "Tamanho: " << datain.size();
        //qDebug() << "msg received";
        if(socket->peerAddress().toString()==clp)
        {
            emit datasent(BytesToInt(intconv.mid(0,2)),BytesToInt(intconv.mid(2,2)),BytesToInt(intconv.mid(4,2)),lote);
        }
        //qDebug() << "V1=" << valor1 << "\nV2=" << valor2 << "\nV3=" << valor3;
    }
    socket->waitForBytesWritten(1000);
    socket->close();
    //delete socket;
    //qDebug() << socketDescriptor << " [I]: " << DataIn;

    //socket->write(DataIn);
}

void server_thread::disconnected()
{
    //qDebug() << socketDescriptor << " Disconnected";
    socket->deleteLater();
    exit(0);
}
