#include "enip.h"

enip::enip(QObject *parent) : QThread(parent)
{
    socket = new QTcpSocket(this);
}

void enip::run()
{
    connect(socket,SIGNAL(disconnected()),this,SLOT(disconnected()),Qt::DirectConnection);
    connect(socket,SIGNAL(connected()),this,SLOT(connected()),Qt::DirectConnection);
    connect(socket,SIGNAL(readyRead()),this,SLOT(ReadyRead()),Qt::DirectConnection);
    //qDebug() << "thread started";

    exec();
}

void enip::conecta()
{
    socket->close();
    socket->connectToHost("192.168.1.253", 80);
}

void enip::connected()
{
    QByteArray firstFrame;
    firstFrame.append("GET /dataview.dat?offset=10&format=4 HTTP/1.1\r\n");
    firstFrame.append("Accept: */*\r\n");
    firstFrame.append("Referer: http://192.168.1.253/dataview.htm?offset=10&format=3\r\n");
    firstFrame.append("Accept: */*\r\n");
    firstFrame.append("Accept-Encoding: gzip, deflate\r\n");
    firstFrame.append("User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; Tablet PC 2.0; .NET CLR 1.1.4322; .NET4.0C; .NET4.0E)\r\n");
    firstFrame.append("Host: 192.168.1.253\r\n");
    firstFrame.append("Connection: Keep-Alive\r\n");
    firstFrame.append("Authorization: Digest username=\"administrator\",realm=\"1763-L16BBB B/9.00\",nonce=\"a4b8c8d7e0f6a7b2c3d2e4f5a4b7c5d2e7f\",uri=\"/dataview.dat?offset=10&format=4\",cnonce=\"d11839a70d52f32d3ef482d5c92e0b04\",nc=00000022,algorithm=MD5,response=\"5b985b1c43d348fea6154be431c2c86d\",qop=\"auth\"\r\n");
    firstFrame.append("\r\n");
    socket->write(firstFrame);
    socket->flush();
    //qDebug() << "[SEND] " << firstFrame.toHex();
    //socket->waitForReadyRead(5000);
    //qDebug() << "[BACK]" << socket->readAll().toHex();
}

void enip::ReadyRead()
{
    //qDebug() << "[BACK]" << socket->readAll();
    QByteArray bdatain(socket->readAll());
    QString datain(bdatain);
    //qDebug() << datain;
    if(bdatain.contains("ST10:48,8,'"))
    {
        //qDebug() << "ST10:48 Encontrado!";
        //qDebug() << bdatain.indexOf("ST10:48");
        qDebug() << "LOTE: " << bdatain.mid((bdatain.indexOf("ST10:48")+11),8);
        emit RetornaLote(datain.mid((bdatain.indexOf("ST10:48")+11),8));
    }
}

void enip::disconnected()
{
    //qDebug() << "[SAIU]";
    //socket->deleteLater();
}
