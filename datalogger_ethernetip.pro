#-------------------------------------------------
#
# Project created by QtCreator 2013-02-20T14:51:09
#
#-------------------------------------------------

QT       += core gui network

TARGET = datalogger_ethernetip
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ethernetip_server.cpp \
    server_thread.cpp \
    qcustomplot/qcustomplot.cpp \
    enip.cpp

HEADERS  += mainwindow.h \
    ethernetip_server.h \
    server_thread.h \
    qcustomplot/qcustomplot.h \
    enip.h

FORMS    += mainwindow.ui

QMAKE_POST_LINK += ./update.sh

RESOURCES += \
    icons.qrc
