#include <QtGui/QApplication>
#include <QtGui/QWSServer>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

#ifdef Q_WS_QWS
    QWSServer::setBackground(QBrush(Qt::black));
    QWSServer::setCursorVisible( false );
    a.setOverrideCursor( QCursor( Qt::BlankCursor ) );
#endif

    MainWindow w;
    w.showFullScreen();

    return a.exec();
}
